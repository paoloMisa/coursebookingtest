import 'course.dart';

// Creating a list of Course objects (Course class)
List<Course> courses = [
  Course(
      name: 'Flutter',
      description: 'Learn everything about Flutter',
      price: 15000),
  Course(
      name: 'JavaScript 101', description: 'Basics of Javascript', price: 5000),
  Course(
      name: 'Django Mastery', description: 'Be a Django Master', price: 25000),
];
