import 'package:flutter/material.dart';

// card template emplate
Widget cardTemplate(course) {
  // Placeholder only
  void learnMoreButton() {
    debugPrint('pressed learn more button');
  }

  return Card(
    margin: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
    // color: Theme.of(context).colorScheme.secondary,
    child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            course.name,
            style: TextStyle(
              fontSize: 18.0,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 6.0,
          ),
          Text(
            course.description,
            style: TextStyle(
              fontSize: 14.0,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            'price: ${course.price}',
            style: TextStyle(
              fontSize: 14.0,
            ),
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
              child: const Text('Learn More'), onPressed: learnMoreButton)
        ],
      ),
    ),
  );
}
