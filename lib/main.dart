import 'package:flutter/material.dart';
import 'card_template.dart';
import 'course_list.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // overrides
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'push notif demo',
        theme: ThemeData.light(),
        home: const MyHomePage(
          title: 'push notification demo',
        ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Returns cards
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset('assets/images/zuittlogo.png'),
      ),
      body: Column(
        children: courses.map((course) => cardTemplate(course)).toList(),
      ),
    );
  }
}
